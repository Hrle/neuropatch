import sys
from neuropatch.parse import parse_markdown_file, convert_to_markdown


def merge_dicts(original_dict, changes_dict):
    """
    Merges two dictionaries of file paths and contents. Changes in the second
    dictionary overwrite those in the first.
    """
    merged_dict = {**original_dict, **changes_dict}
    return merged_dict


def merge_code(original_code, changes_code, output_file_path):
    """
    Merges two markdown files of code snippets. Changes in the second file
    overwrite those in the first.
    """
    original = parse_markdown_file(original_code)
    changed = parse_markdown_file(changes_code)
    merged = merge_dicts(original, changed)
    convert_to_markdown(merged, output_file_path)


if __name__ == "__main__":
    merge_code(sys.argv[1], sys.argv[2], sys.argv[3])
