import sys
import openai


def upload_file(api_key, path_to_bundle):
    openai.api_key = api_key

    return openai.File.create(file=path_to_bundle, purpose="search")


def call_openai_gpt3(api_key, path_to_bundle, prompt):
    openai.api_key = api_key

    upload_file(api_key, path_to_bundle)

    response = openai.ChatCompletion.create(
        engine="gpt-3.5-turbo", prompt=prompt
    )
    print(response.choices[0].message.content.strip())


if __name__ == "__main__":
    if len(sys.argv) == 3:
        print(upload_file(sys.argv[1], sys.argv[2]))
    else:
        call_openai_gpt3(sys.argv[1], sys.argv[2], sys.argv[3])
