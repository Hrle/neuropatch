import re
import sys
import os
import json
from collections import defaultdict


def parse_markdown_file(md_file_path):
    """
    Parses a markdown file into a dictionary where keys are file paths and
    values are file contents.
    """
    with open(md_file_path, "r", encoding="utf-8") as file:
        content = file.read()

    pattern = r"- (.*?)\n\n```\n(.*?)\n```"
    matches = re.findall(pattern, content, re.DOTALL)

    result = defaultdict(str)
    for match in matches:
        result[match[0]] = match[1]

    return result


def convert_to_markdown(file_dict, output_file_path):
    """
    Converts a dictionary of file paths and contents to a markdown file.
    """
    with open(output_file_path, "w", encoding="utf-8") as output_file:
        for file_path, file_content in file_dict.items():
            output_file.write("- " + file_path + "\n\n")
            output_file.write("```\n" + file_content + "\n```\n\n")


if __name__ == "__main__":
    if sys.argv[1].endswith(".md"):
        parsed = parse_markdown_file(sys.argv[1])

        with open(sys.argv[2], "w", encoding="utf-8") as f:
            json.dump(parsed, f)
    else:
        if os.stat(sys.argv[1]).st_size != 0:
            with open(sys.argv[1], "r", encoding="utf-8") as f:
                convert_to_markdown(json.load(f), sys.argv[2])
        else:
            print(f"File {sys.argv[1]} is empty.")
