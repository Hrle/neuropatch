import sys
import os
from neuropatch.parse import parse_markdown_file
from pathlib import Path


def apply_changes_to_repository(markdown_file_path, repository_path):
    """
    Applies the changes in the given markdown file to the given repository.
    """
    changes_dict = parse_markdown_file(markdown_file_path)
    repository_path = Path(repository_path)

    for file_path, new_content in changes_dict.items():
        full_path = repository_path / file_path

        os.makedirs(full_path.parent, exist_ok=True)

        with open(full_path, "w", encoding="utf-8") as file:
            file.write(new_content)


if __name__ == "__main__":
    apply_changes_to_repository(sys.argv[1], sys.argv[2])
