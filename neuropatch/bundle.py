import sys
import os
from pathlib import Path
import pathspec


def bundle_repository(repository_path, output_file_path):
    """
    Bundles the repository at the given path into a single markdown file.
    """
    print(
        "Bundling repository at "
        + repository_path
        + " into "
        + output_file_path
    )

    repository_path = Path(repository_path)
    with open(repository_path / ".gitignore", "r", encoding="utf-8") as f:
        gitignore = pathspec.PathSpec.from_lines("gitwildmatch", f)

    source_file_extensions = [".py"]
    with open(output_file_path, "w", encoding="utf-8") as output_file:
        output_file.write(
            "# " + os.path.basename(os.path.abspath(repository_path)) + "\n\n"
        )
        for dirpath, _, filenames in os.walk(repository_path):
            for filename in filenames:
                file_path = (Path(dirpath) / filename).as_posix()
                if any(
                    file_path.endswith(ext) for ext in source_file_extensions
                ) and not gitignore.match_file(file_path):
                    output_file.write("- " + file_path + "\n\n")
                    with open(file_path, "r", encoding="utf-8") as file:
                        file_content = file.read()
                        output_file.write("```\n" + file_content + "\n```\n\n")


if __name__ == "__main__":
    bundle_repository(sys.argv[1], sys.argv[2])
